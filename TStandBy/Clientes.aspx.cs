﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TStandBy
{
    public partial class Clientes : System.Web.UI.Page
    {
        static string sPara = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=" + HttpContext.Current.Server.MapPath("~/") + @"App_Data\Clientes.mdf;Integrated Security=True";

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static MessageCallback table0(string f)
        {
            MessageCallback Retorno = new MessageCallback();

            string sTable = string.Empty;
            string sSql = string.Empty;
            System.Text.StringBuilder sbRetorno = new System.Text.StringBuilder("");
            DBAccess ADados = new DBAccess();

            try
            {
                sSql = "select * from [cliente]";
                DataTable dtClientes = ADados.returnSelec(sSql, sPara);
                foreach (DataRow row in dtClientes.Rows)
                {
                    var status = Convert.ToInt32(row["status_cliente"]) == 1 ? "Ativo" : "Desativado";
                    sTable += "<tr>" +
                             "<td ><span>" + row["id"] + "</span></td>" +
                             "<td>" + row["razao_social"] + "</td>" +
                             "<td>" + row["cnpj"] + "</td>" +
                             "<td>" + row["data_fundacao"] + "</td>" +
                             "<td>" + row["capital"] + "</td>" +
                             "<td>" + status + "</td>" +
                             "<td><a href=\"#\" onClick=\"openEditar(" + row["id"] + ")\">ALT</a> ||| <a href=\"#\" onClick=\"openExcluir(" + row["id"] + ")\">EX</a></td>" +
                             "<td>BTN</td>" +
                             "<td></td>" +
                             "</tr>";
                }

                Retorno.Success = true;
                Retorno.Message = "";
                Retorno.Data = sTable;
            }
            catch (Exception e)
            {
                Retorno.Success = false;
                Retorno.Message = "Erro ao buscar registros. " + e.Message;
            }
            return Retorno;
        }

        public void validacoes()
        {
            if (txtRazaoSocail.Text == string.Empty)
            {
                throw new Exception("Informe a Razão Social");
            }
            if (txtCapital.Text == string.Empty)
            {
                throw new Exception("Informe o Capital");
            }
            if (txtDataFundacao.Text == string.Empty)
            {
                throw new Exception("Informe a Data de Fundação");
            }
            if (txtCNPJ.Text == string.Empty)
            {
                throw new Exception("Informe o CNPJ");
            }

        }
        public void limpaCampos()
        {
            txtCapital.Text = "";
            txtCNPJ.Text = "";
            txtDataFundacao.Text = "";
            txtRazaoSocail.Text = "";

            txtCapitalUP.Text = "";
            txtCnpjUP.Text = "";
            txtDataUP.Text = "";
            txtRazaoUP.Text = "";
        }

        #region CRUD
        public void createCliente(object sender, EventArgs e)
        {
            DBAccess ADados = new DBAccess();
            string sInsert = string.Empty;
            string sSql = string.Empty;

            try
            {
                validacoes();
                bool cnpj = false;
                sSql = "SELECT [cnpj] FROM [cliente] WHERE [cnpj] like '" + txtCNPJ.Text + "'";
                DataTable buscaCNPJ = ADados.returnSelec(sSql, sPara);
                foreach (DataRow row in buscaCNPJ.Rows)
                {
                    cnpj = true;
                    break;
                }

                // Validação CNPJ
                if (cnpj == false)
                {
                    //
                    int quarentena = 0;

                    decimal capital = Convert.ToDecimal(txtCapital.Text);
                    char classificacao = 'C';

                    // Validação Quarentena
                    if (Convert.ToDateTime(txtDataFundacao.Text) > DateTime.Now.AddDays(-365))
                    {
                        quarentena = 1;
                    }

                    // Validação Capital
                    if (capital > 100000 && capital <= 1000000)
                    {
                        classificacao = 'B';
                    }
                    else if (capital > 1000000)
                    {
                        classificacao = 'A';
                    }

                    sInsert = "INSERT INTO [dbo].[cliente] ([razao_social], [cnpj], [data_fundacao], [capital], [quarentena], [status_cliente], [classificacao]) VALUES (N'" + txtRazaoSocail.Text + "', N'" + txtCNPJ.Text + "', N'" + txtDataFundacao.Text + "', CAST(" + txtCapital.Text + " AS Decimal(18, 0)), " + quarentena + ", " + rdbStatus.SelectedItem.Value + ", N'" + classificacao + "')";
                    ADados.insert(sInsert, sPara);

                    limpaCampos();
                }
            }
            catch (Exception ex)
            {
            }
        }

        public void updateCliente(object sender, EventArgs e)
        {
            DBAccess ADados = new DBAccess();
            string sInsert = string.Empty;

            try
            {
                //
                int quarentena = 0;

                decimal capital = Convert.ToDecimal(txtCapitalUP.Text);
                char classificacao = 'C';

                // Validação Quarentena
                if (Convert.ToDateTime(txtDataUP.Text) > DateTime.Now.AddDays(-365))
                {
                    quarentena = 1;
                }

                // Validação Capital
                if (capital > 100000 && capital <= 1000000)
                {
                    classificacao = 'B';
                }
                else if (capital > 1000000)
                {
                    classificacao = 'A';
                }
                sInsert = "UPDATE [cliente] SET [razao_social] = '" + txtRazaoUP.Text + "', [cnpj] = '" + txtCnpjUP.Text + "', [data_fundacao] = " + txtDataUP.Text + ", [capital] =CAST(" + txtCapitalUP.Text + " AS Decimal(18, 0)), [quarentena] = " + quarentena + ", [status_cliente] = " + rdbUpdate.SelectedItem.Value + ", [classificacao] = '" + classificacao + "' WHERE [id] = " + HttpContext.Current.Session["cod_cliente"];
                ADados.insert(sInsert, sPara);
                limpaCampos();
            }
            catch (Exception ex)
            {

            }
        }

        public void deleteCLiente(object sender, EventArgs e)
        {
            string sDelete = string.Empty;
            DBAccess ADados = new DBAccess();
            try
            {
                sDelete = "delete from [cliente] where [id] = " + HttpContext.Current.Session["cod_cliente"];
                ADados.insert(sDelete, sPara);
            }
            catch (Exception ex)
            {

            }
        }

        #endregion

        [WebMethod]
        public static void codCliente(string f)
        {
            try
            {
                HttpContext.Current.Session["cod_cliente"] = f;
            }
            catch (Exception e)
            {

            }
        }

    }
}