﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Master.Master" AutoEventWireup="true" CodeBehind="Clientes.aspx.cs" Inherits="TStandBy.Clientes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <!-- DATATABLES-->
    <!--DataTable CSS-->
    <link href="../css/dataTables.bootstrap.min.css" rel="stylesheet" />
    <link href="../css/responsive.bootstrap.min.css" rel="stylesheet" />
    <link href="../css/select.dataTables.min.css" rel="stylesheet" />
    <link href="/css/jquery.dataTables.min.css" rel="stylesheet" />
    <!--DataTable JS-->
    <script type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="../js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="../js/dataTables.select.min.js"></script>
    <script type="text/javascript" src="../js/responsive.bootstrap.min.js"></script>
    <!-- DATATABLES-->
    <!--MODAL-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>


    <script type="text/javascript">

        $(document).ready(function () {
            var tb = "#tbClientes";
            tbc = createDataTable(tb);
            CarregaClientes();
            document.getElementById('<%= pnlRegistro.ClientID %>').style.display = 'none';
            document.getElementById('<%= pnlUpdate.ClientID %>').style.display = 'none';
        });

        function voltar() {
            document.getElementById('<%= pnlRegistro.ClientID %>').style.display = 'none';
            document.getElementById('<%= pnlUpdate.ClientID %>').style.display = 'none';
            document.getElementById('<%= pnlPrincipal.ClientID %>').style.display = 'block';
        }

        function novo() {
            document.getElementById('<%= pnlRegistro.ClientID %>').style.display = 'block';
            document.getElementById('<%= pnlUpdate.ClientID %>').style.display = 'none';
            document.getElementById('<%= pnlPrincipal.ClientID %>').style.display = 'none';
        }

        function CarregaClientes() {
            var tb = "#tbClientes";
            var object = new Object();
            object.f = $("#form1").serialize();
            var dataSID = JSON.stringify(object);
            var fid = 'Clientes.aspx';
            $(tb + " tbody").html('<td colspan="' + $(tb).find("thead:first th").length + '"><center><img src="../images/loading.gif" /></center></td>');
            $.ajax
                ({
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    url: fid + '/table0',
                    dataType: "json",
                    data: dataSID,
                    success: function (data) {
                        $(tb).DataTable().clear();
                        if (data.d.Success) {
                            var dados = data.d.Data;
                            linhas = dados.split("<tr>");
                            for (i = 0; i < linhas.length; i++) {

                                if (linhas[i] != '') {
                                    var campos = linhas[i].split("<td>");
                                    $('#tbClientes').DataTable().row.add([
                                        campos[0],
                                        campos[1],
                                        campos[2],
                                        campos[3],
                                        campos[4],
                                        campos[5],
                                        campos[6],
                                        campos[7],
                                        campos[8]
                                    ]);
                                }
                            }
                        }
                        $(tb).DataTable().draw(true);
                    },
                    error: function (data) {
                        openModal('Erro ao carregar os dados');
                    }
                })
        };


        function createDataTable(tb, design, hiddenCols, pagingType, search, selectType, information, chkTarget, colDef) {
            if (typeof tb === 'undefined' || !tb) { tb = '#tableSearch'; }
            if (typeof hiddenCols === 'undefined' || !hiddenCols) { hiddenCols = [0]; }
            if (typeof pagingType === 'undefined') { pagingType = true; }
            if (typeof search === 'undefined') { search = true; }
            if (typeof selectType === 'undefined') { selectType = 'multi'; }
            if (typeof information === 'undefined') { information = true; }
            if (typeof chkTarget === 'undefined' || !chkTarget) { chkTarget = -1; }
            if (typeof colDef === 'undefined' || !colDef) { colDef = ""; }
            if (typeof design === 'undefined' || !design) {
                design = '<"top"f>lrtip<"clear"><"clear"><"bottom"B>';
            }
            var btBasics = [];
            $(tb).DataTable({
                dom: design,
                paging: pagingType,
                select: true,
                searching: search,
                info: information,
                autoWidth: false,
                "scrollX": true,
                buttons: [
                    btBasics
                ],
                columnDefs: [{
                    orderable: false,
                    className: 'select-checkbox',
                    width: "1%",
                    // targets: chkTarget
                }, {
                    targets: hiddenCols,
                    width: "1%",
                    orderable: false,
                    visible: false
                }, colDef],
                //select: {
                //    style: selectType,
                //    selector: 'td:not(:has(a,div))',
                //    info: false
                //},
                language: {
                    select: {
                        rows: {
                            _: "",
                            0: "",
                            1: ""
                        }
                    },
                    buttons: {
                        selectAll: "Marcar todos",
                        selectNone: "Desmarcar todos"
                    }
                },
                order: [[1, 'asc']]
            });

            var tbc = $(tb).dataTable().api();
            if (tbc.buttons().count() > 1) {
                tbc.button(1).enable(false);
                if (tbc.button(".btn-danger").count() > 0) {
                    tbc.button(".btn-danger").enable(false);
                }
            }

            $(tb + "_filter input")
                .unbind()
                .bind("input", function (e) {
                    if (this.value == "") {
                        tbc.search("").draw();
                    } else {
                        tbc.search(this.value).draw();
                    }
                    tbc.rows({ filter: 'applied' }).deselect();
                    return;
                });
        };



        function openEditar(cod) {
            document.getElementById('<%= pnlPrincipal.ClientID %>').style.display = 'none';
            document.getElementById('<%= pnlUpdate.ClientID %>').style.display = 'block';

            var object = new Object();
            object.f = cod;
            var dataSID = JSON.stringify(object);
            var fid = 'Clientes.aspx';
            $.ajax
                ({
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    url: fid + '/codCliente',
                    dataType: "json",
                    data: dataSID,
                    success: function (data) {
                        if (data.d.Success) {
                        }

                    },
                    error: function (data) {
                        openModal('Erro ao carregar os dados');
                    }
                })
        };

        function openExcluir(cod) {
            $("#modalExcluir").modal();


            var object = new Object();
            object.f = cod;
            var dataSID = JSON.stringify(object);
            var fid = 'Clientes.aspx';
            $.ajax
                ({
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    url: fid + '/codCliente',
                    dataType: "json",
                    data: dataSID,
                    success: function (data) {
                        if (data.d.Success) {
                           
                        }

                    },
                    error: function (data) {
                        openModal('Erro ao carregar os dados');
                    }
                })
        };

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <asp:Panel runat="server" ID="pnlPrincipal">
        <asp:Button runat="server" ID="btnNovo"  class="btn btn-success btn-rounded btn-fw" OnClientClick="novo();return false;" Text="Novo" />
        <table id="tbClientes">
            <thead>
                <tr>
                    <th></th>
                    <th>Razão Social</th>
                    <th>CNPJ</th>
                    <th>Data de Fundação</th>
                    <th>Capital</th>
                    <th>Status</th>
                    <th>Ação</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlRegistro" Visible="true">
        <asp:Button ID="btnVoltar" runat="server" Text="Voltar" class="btn btn-warning btn-rounded btn-fw" OnClientClick="voltar()" />
        <h1>Cadastro de Cliente</h1>
        <label>Razão Social</label>
        <asp:TextBox runat="server" ID="txtRazaoSocail" type="text" placeholder="Razão Social" ></asp:TextBox>
        <br />
        <label>CNPJ</label>
        <asp:TextBox runat="server" ID="txtCNPJ" type="text" placeholder="CNPJ" ></asp:TextBox>
        <br />
        <label>Data de Fundação</label>
        <asp:TextBox runat="server" ID="txtDataFundacao" type="date" placeholder="Data de Fundação" ></asp:TextBox>
        <br />
        <label>Capital</label>
        <asp:TextBox runat="server" ID="txtCapital" type="number" min="0" placeholder="Capital" ></asp:TextBox>
        <br />
        <asp:RadioButtonList ID="rdbStatus" runat="server">
            <asp:ListItem Selected="True" Text="Ativo" Value="1" />
            <asp:ListItem Text="Desativado" Value="0" />
        </asp:RadioButtonList>
        <br />
        <asp:Button ID="btnGravar" runat="server" OnClick="createCliente" class="btn btn-success btn-rounded btn-fw" Text="Gravar" />
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlUpdate" Visible="true">
        <asp:Button ID="Button2" runat="server" Text="Voltar" class="btn btn-warning btn-rounded btn-fw" OnClientClick="voltar()" />
        <h1>Cadastro de Cliente</h1>
        <label>Razão Social</label>
        <asp:TextBox runat="server" ID="txtRazaoUP" type="text" placeholder="Razão Social" ></asp:TextBox>
        <br />
        <label>CNPJ</label>
        <asp:TextBox runat="server" ID="txtCnpjUP" type="text" placeholder="CNPJ" ></asp:TextBox>
        <br />
        <label>Data de Fundação</label>
        <asp:TextBox runat="server" ID="txtDataUP" type="date" placeholder="Data de Fundação" ></asp:TextBox>
        <br />
        <label>Capital</label>
        <asp:TextBox runat="server" ID="txtCapitalUP" type="number" min="0" placeholder="Capital" ></asp:TextBox>
        <br />
        <asp:RadioButtonList ID="rdbUpdate" runat="server">
            <asp:ListItem Selected="True" Text="Ativo" Value="1" />
            <asp:ListItem Text="Desativado" Value="0" />
        </asp:RadioButtonList>
        <br />
        <asp:Button ID="btnUpdate" runat="server" OnClick="updateCliente" class="btn btn-success btn-rounded btn-fw" Text="Gravar" />
    </asp:Panel>
    <div class="modal fade" id="modalExcluir" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">
                       Excluir Cliente
                    </h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <p>
                        Você tem certeza que deseja excluir o cliente?
                    </p>
                </div>
                <div class="modal-footer">
                    <asp:Button ID="Button1" runat="server" class="btn btn-danger btn-rounded btn-fw" OnClick="deleteCLiente" Text="Excluir" />
                    <button type="button" class="btn btn-success btn-rounded btn-fw" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
