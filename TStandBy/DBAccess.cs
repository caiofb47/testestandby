﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace TStandBy
{
    public class DBAccess
    {

        public DataTable returnSelec(string select, string connection)
        {
            DataTable retorno = new DataTable();
            SqlConnection conn = new SqlConnection(connection);
            try
            {
                conn.Open();
                SqlDataAdapter sda = new SqlDataAdapter(select, conn);
                sda.Fill(retorno);
                conn.Close();
            }
            catch (Exception ex)
            {
                throw new Exception("Erro no retorno do banco!");
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    // Se a conexão ainda estiver aberta, o finally fecha
                    conn.Close();
                }
            }
            return retorno;
        }




        public MessageCallback insert(string insert, string connection)
        {
            MessageCallback retorno = new MessageCallback();
            SqlConnection conn = new SqlConnection(connection);
            try
            {
                conn.Open();
                SqlCommand CmdSql = new SqlCommand(insert, conn);
                CmdSql.ExecuteNonQuery();
                conn.Close();
                retorno.Success = true;
            }
            catch (Exception ex)
            {
                retorno.Success = false;
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    // Se a conexão ainda estiver aberta, o finally fecha
                    conn.Close();
                }
            }
            return retorno;
        }



    }
}